Rails.application.routes.draw do
  root to: 'games#ui'
  post "/hit", to: "games#save_hit"

  resources :games, only: [:create] do
    collection do
      get :current
      post :finish
    end
  end

end
