class GamesController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :save_hit

  def ui
    @current_game = current_game

    if @current_game
      @current_turn = GameService.new(@current_game).current_turn
    else
      @last_gname = Game.where(complete: true).last
    end
  end

  def current
    @current_game = current_game

    if @current_game
      service = GameService.new(@current_game)
      current_turn = service.current_turn

      players = {}
      @current_game.players.map do |player|
        players[player.id] = {
          name:   player.name,
          score:  player.score.to_i,
          winner: !!player.winner
        }
      end

      throw_mtx = {}
      service.get_throw_mtx.each do |player_id, frames_hash|
        throw_mtx[player_id] ||= {}
        scores = service.player_scores(player_id)
        frames_hash.each do |fn, hits|
          throw_mtx[player_id][fn] = {
            hits: hits.map(&:pins),
            score: scores[fn],
            is_strike: !!hits.first.try(:is_strike),
            is_spare: !!hits.second.try(:is_spare)
          }
        end
      end

      render json: {
        game_id: @current_game.id,
        start_at: @current_game.start_at,
        complete: @current_game.complete,
        players: players,
        frames: throw_mtx,
        current_turn: {
          frame: current_turn[:frame],
          player_id: current_turn[:player].try(:id),
          player_name: current_turn[:player].try(:name),
          max_pins: current_turn[:max_pins]
        },
      }
    else
      render json: nil, status: 404
    end
  end

  def create
    if current_game.present?
      render json: {success: false, messages: ["game already started"]}, status: 422
      return
    end

    @game = Game.new(game_params)

    if @game.save
      render json: {success: true, game_id: @game.id}, status: 201
    else
      render json: {success: false, messages: @game.errors.full_messages}, status: 422
    end
  end

  def finish
    if current_game
      current_game.update(complete: true)
      render json: {success: true, game_id: current_game.id}, status: 202
    else
      render json: {success: false, messages: ["Game not started"]}, status: 422
    end
  end

  def save_hit
    if params[:score].blank?
      render json: {success: false, messages: ["Score parameter is missing"]}, status: 422
      return
    end

    unless current_game
      render json: {success: false, messages: ["Game not started"]}, status: 422
      return
    end

    GameService.new(current_game).save_hit(params[:score].to_i)

    render json: {success: true}, status: 202
  end

  private

  def current_game
    @current_game ||= Game.where(complete: false).first
  end

  def game_params
    params.require(:game).permit(:player_names)
  end
end
