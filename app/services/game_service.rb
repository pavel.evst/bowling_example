class GameService
  attr_accessor :game

  def initialize(game)
    @game = game
  end

  def current_turn
    current_frame, current_player_id = nil, nil
    max_pins = 10

    player_ids = @game.player_ids.sort
    throws = @game.ball_throws.order("player_id, frame").to_a

    # build matrix as [player_id][frame_no] = [#<BallThrow>, #<BallThrow>]
    throw_mtx = _throw_mtx(throws, player_ids)

    # check for unfinished frame
    unfinished_frame = _find_unfinished_frame(throw_mtx)
    if unfinished_frame
      current_player_id, current_frame = unfinished_frame
      max_pins = _get_max_pins(throw_mtx, current_player_id, current_frame)
    else
      if throws.present? # when no unfinished frame
        unfinished_round_frame = _player_in_unfinished_round(throw_mtx)
        if unfinished_round_frame
          current_player_id, current_frame = unfinished_round_frame
        end
      else # when no previous hits
        current_player_id = player_ids.first
        current_frame = 1
      end
    end

    return {
      player: @game.players.find_by(id: current_player_id),
      frame: current_frame,
      max_pins: max_pins
    }
  end

  def save_hit(score)
    turn = self.current_turn
    Rails.logger.info("Saving score for #{turn[:player].name} frame #{turn[:frame]} - #{score}")

    is_spare = false
    if score < 10
      last_hit = BallThrow.find_by(game: @game, player: turn[:player], frame: turn[:frame])
      if last_hit && last_hit.pins + score == 10
        is_spare = true
      end
    end

    BallThrow.transaction do
      BallThrow.create!(
        game: @game,
        player: turn[:player],
        pins: score,
        frame: turn[:frame],
        is_strike: score == 10,
        is_spare: is_spare
      )

      turn[:player].update(score: player_scores(turn[:player].id).values.sum)

      # Check if game is complete
      if turn[:frame] == 10 && self.current_turn[:frame] == nil
        @game.update(complete: true)
        @game.players.order("score desc").first.update(winner: true)
        @game.reload
      end
    end
  end

  def player_scores(player_id)
    throws = @game.ball_throws.where(player_id: player_id)

    scores = {}
    throws.each_with_index do |th, index|
      scores[th.frame] ||= 0
      scores[th.frame] += th.pins

      if th.is_strike?
        scores[th.frame] += throws[index + 1].pins if throws[index + 1]
        scores[th.frame] += throws[index + 2].pins if throws[index + 2]
      elsif th.is_spare?
        scores[th.frame] += throws[index + 1].pins if throws[index + 1]
      end
    end

    scores
  end

  def get_throw_mtx
    player_ids = @game.player_ids.sort
    throws = @game.ball_throws.order("player_id, frame").to_a

    _throw_mtx(throws, player_ids)
  end

  private

  def _throw_mtx(throws, player_ids)
    throw_mtx = {}
    player_ids.each {|player_id| throw_mtx[player_id] = {} }

    throws.each do |th|
      throw_mtx[th.player_id][th.frame] ||= []
      throw_mtx[th.player_id][th.frame] << th
    end

    throw_mtx
  end

  def _player_in_unfinished_round(throw_mtx)
    player_ids = throw_mtx.keys

    1.upto(10) do |fn|
      current_round_player = player_ids.detect do |player_id|
        !throw_mtx[player_id][fn]
      end

      if current_round_player
        return [current_round_player, fn]
      end
    end

    nil
  end

  def _find_unfinished_frame(throw_mtx)
    player_ids = throw_mtx.keys

    player_ids.each do |player_id|
      1.upto(10) do |fn|
        frame = throw_mtx[player_id][fn]

        # extend last frame if it's strike
        if fn == 10 && frame && frame.first.is_strike && frame.size < 3
          return [player_id, fn]
        end

        # extend last frame if it's spare
        if fn == 10 && frame && frame.second && frame.second.is_spare && frame.size < 3
          return [player_id, fn]
        end

        if frame && frame.size == 1 && !frame.first.is_strike
          return [player_id, fn]
        end
      end
    end

    nil
  end

  def _get_max_pins(throw_mtx, current_player_id, current_frame)
    frame = throw_mtx[current_player_id][current_frame]

    # strike in last frame
    if current_frame == 10 && frame.first.is_strike?
      if frame.second
        return 10 - frame.second.pins
      else
        return 10
      end
    # spare in last frame
    elsif current_frame == 10 && frame.second && frame.second.is_spare
      return 10
    # normal unfinished frame
    elsif frame.present?
      return 10 - frame.first.pins
    end
  end
end
