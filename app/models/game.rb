class Game < ApplicationRecord

  has_many :players
  has_many :ball_throws

  accepts_nested_attributes_for :players

  attribute :player_names, :text

  validates :player_names, presence: true, on: :create

  def initialize(*args)
    super(*args)

    if new_record?
      if self[:start_at].blank?
        write_attribute(:start_at, Time.now)
      end
    end
  end

  def player_names=(values)
    names = values.split("\n").select(&:present?)
    self.players_attributes = names.map do |name|
      {name: name.strip }
    end

    @player_names = names.present? ? values : ""
  end

  def player_names
    @player_names
  end

end
