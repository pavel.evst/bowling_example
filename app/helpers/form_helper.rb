module FormHelper

  def error_messages(resource)
    return "" if resource.errors.empty?

    error_messages_element(resource.errors.full_messages.uniq, resource.class.model_name.human.downcase)
  end

  def error_messages_element(errors, resource_name)
    return "" if errors.empty?

    messages = errors.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t("errors.messages.not_saved", count: errors.size, resource: resource_name)

    html = <<-HTML
    <div id="error_explanation" class="alert alert-danger">
      <h4>#{sentence}</h4>
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end

end
