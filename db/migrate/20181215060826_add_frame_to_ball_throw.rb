class AddFrameToBallThrow < ActiveRecord::Migration[5.2]
  def change
    add_column :ball_throws, :frame, :integer
  end
end
