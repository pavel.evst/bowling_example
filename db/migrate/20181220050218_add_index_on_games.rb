class AddIndexOnGames < ActiveRecord::Migration[5.2]
  def change
    add_index :games, :complete
  end
end
