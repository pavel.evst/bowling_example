class CreateGameParticipants < ActiveRecord::Migration[5.2]
  def change
    create_table :game_participants do |t|
      t.string :name
      t.integer :score
      t.boolean :winner
      t.references :game

      t.timestamps
    end
  end
end
