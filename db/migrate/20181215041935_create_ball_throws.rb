class CreateBallThrows < ActiveRecord::Migration[5.2]
  def change
    create_table :ball_throws do |t|
      t.references :game, foreign_key: true
      t.references :game_participant, foreign_key: true
      t.integer :pins
      t.boolean :is_strike
      t.boolean :is_spare

      t.timestamps
    end
  end
end
