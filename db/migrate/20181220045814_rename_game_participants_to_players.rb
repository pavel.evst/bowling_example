class RenameGameParticipantsToPlayers < ActiveRecord::Migration[5.2]
  def change
    rename_table :game_participants, :players
    rename_column :ball_throws, :game_participant_id, :player_id
  end
end
