class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.datetime :start_at
      t.boolean :complete, default: false, null: false

      t.timestamps
    end
  end
end
