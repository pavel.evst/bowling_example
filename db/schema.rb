# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_20_050218) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ball_throws", force: :cascade do |t|
    t.bigint "game_id"
    t.bigint "player_id"
    t.integer "pins"
    t.boolean "is_strike"
    t.boolean "is_spare"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "frame"
    t.index ["game_id"], name: "index_ball_throws_on_game_id"
    t.index ["player_id"], name: "index_ball_throws_on_player_id"
  end

  create_table "games", force: :cascade do |t|
    t.datetime "start_at"
    t.boolean "complete", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["complete"], name: "index_games_on_complete"
  end

  create_table "players", force: :cascade do |t|
    t.string "name"
    t.integer "score"
    t.boolean "winner"
    t.bigint "game_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id"], name: "index_players_on_game_id"
  end

  add_foreign_key "ball_throws", "games"
  add_foreign_key "ball_throws", "players"
end
