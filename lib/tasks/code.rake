
namespace :code do
  IGNORE_PATHS = [
    "app/assets/javascripts/lib",
  ]
  EXTENSIONS = %w{rake rb scss js es6}

  def test_file_spaces(file)
    content = file.open('r:utf-8', &:read)
    {
      file: file.to_s,
      tabs: content.scan(/\t/).size,
      tail_spaces: content.scan(/[ \t]+$/).size,
      tail_CR: content.scan("\r\n").size,
      end_blank: content[-1] == "\n"
    }
  rescue => error
    puts "FILE: #{file}"
    raise error
  end

  def ignore_file?(file)
    file = file.to_s
    IGNORE_PATHS.each do |path|
      if file.start_with?(path) || file.start_with?(Rails.root.join(path).to_s)
        return true
      end
    end
    return false
  end

  desc "Find files with wrong white spaces"
  task find_spaces: :environment do
    files = []
    Pathname.glob(Rails.root.join("**/*.{#{EXTENSIONS.join(",")}}")).each do |file|
      next if ignore_file?(file)
      files << test_file_spaces(file)
    end
    files.each do |file|
      if file[:tabs] > 0 || file[:tail_spaces] > 0 || !file[:end_blank] || file[:tail_CR] > 0
        puts "#{file[:file].sub(Rails.root.to_s + '/', '')} t:#{file[:tabs]}, ts: #{file[:tail_spaces]}, end line: #{file[:end_blank]}, tCR: #{file[:tail_CR]}"
      end
    end
  end

  def fix_file_spaces(file)
    content = file.open('r:utf-8', &:read)
    content.gsub!("\r\n", "\n")
    content.gsub!(/\t/, '    ')
    content.gsub!(/[ \t]+$/, '')
    if content[-1] != "\n"
      content += "\n"
    end
    file.open('w:utf-8') do |f|
      f.write(content)
    end
  end

  desc "Fix files with wrong white spaces"
  task fix_spaces: :environment do
    Pathname.glob(Rails.root.join("**/*.{#{EXTENSIONS.join(",")}}")).each do |file|
      next if ignore_file?(file)
      res = test_file_spaces(file)
      if res[:tabs] > 0 || res[:tail_spaces] > 0 || !res[:end_blank] || res[:tail_CR] > 0
        puts "Writting: #{file.to_s.sub(Rails.root.to_s + '/', '')}"
        fix_file_spaces(file)
      end
    end
  end
end
