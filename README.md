# Example rails app for bowling API

### How to run app:

```sh
bundle
rake db:create db:migrate
rails s
# open http://localhost:3000 in browser
```

### How to run test:

```sh
bundle
rake db:create db:migrate RAILS_ENV=test
rake test
```

## API

* `GET /games/current` - complete state of current game, null if no active game
* `POST /games game[player_names]=Bob\nAlice` - create game
* `POST /hit score=8` - record ball throwing
* `POST /games/finish` - finish game prematurely
