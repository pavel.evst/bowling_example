ENV["RAILS_ENV"] = "test"
require_relative "../config/environment"

require "rails/test_help"
require "minitest/autorun"
require "minitest/reporters"
require 'database_cleaner'

require "rails/test_unit/reporter"
Rails::TestUnitReporter.class_eval do
  def format_rerun_snippet(result)
    location, line = result.source_location
    "#{executable} #{relative_path_for(location)}:#{line}"
  end
end

Minitest::Reporters.use!(
  Minitest::Reporters::DefaultReporter.new(color: true)
)

# show full backtrace on error
def MiniTest.filter_backtrace(bt)
  bt
end

DatabaseCleaner.allow_remote_database_url = true
DatabaseCleaner.strategy = :truncation

class Minitest::Spec
  before :each do
    DatabaseCleaner.start
  end

  after :each do
    DatabaseCleaner.clean
  end
end

module MiniTest::Assertions
  # Same as original, but swap arguments:
  # assert_equal(user.age, 28)
  def assert_equal(act, exp, msg = nil)
    msg = message(msg, E) { diff exp, act }
    assert(exp == act, msg)
  end
end


# Clean database on load
DatabaseCleaner.clean


# add response.json_body
ActionDispatch::TestResponse.class_eval do
  def json_body
    if body.present?
      JSON.parse(body).deep_symbolize_keys
    else
      body
    end
  end
end
