require_relative "../test_helper"

class GamesControllerTest < ActionDispatch::IntegrationTest
  make_my_diffs_pretty!

  test "should validate POST /games" do
    post "/games", params: {game: {player_names: ""}}

    assert_equal(response.code, "422")
    assert_equal(response.json_body, {success: false, messages: ["Player names can't be blank"]})
  end

  test "should start game" do

    post "/games", params: {game: {player_names: "Alice\nBob"}}

    game = Game.last

    assert_equal(response.code, "201")
    assert_equal(response.json_body, {success: true, game_id: game.id})

    assert_equal(game.players.map(&:name).sort, ["Alice", "Bob"])
  end

  test "should validate POST /games/finish" do
    post "/games/finish"

    assert_equal(response.code, "422")
    assert_equal(response.json_body, {success: false, messages: ["Game not started"]})
  end

  test "should finish game" do
    post "/games", params: {game: {player_names: "Alice\nBob"}}
    game = Game.last
    post "/games/finish"

    assert_equal(response.code, "202")
    assert_equal(response.json_body, {success: true, game_id: game.id})

    assert_equal(game.reload.complete, true)
  end

  test "should validate POST /hit" do
    post "/hit"

    assert_equal(response.code, "422")
    assert_equal(response.json_body, {success: false, messages: ["Score parameter is missing"]})

    post "/hit", params: {score: 5}

    assert_equal(response.code, "422")
    assert_equal(response.json_body, {success: false, messages: ["Game not started"]})
  end

  test "should save hit" do
    post "/games", params: {game: {player_names: "Alice\nBob"}}

    post "/hit", params: {score: 5}

    assert_equal(response.code, "202")
    assert_equal(response.json_body, {success: true})

    assert_equal(Game.last.ball_throws.last.pins, 5)
  end

  test "full game state" do
    post "/games", params: {game: {player_names: "Alice\nBob"}}
    post "/hit", params: {score: 5}

    get "/games/current"
    game = Game.last

    players_info = {}
    game.players.order("id").each do |player|
      players_info[player.id.to_s.to_sym] = {
        name: player.name, score: player.score.to_i, winner: false
      }
    end

    frames = {}
    pid = game.players.first.id.to_s.to_sym
    frames[pid] = {
      "1": { hits: [5], score: 5, is_strike: false, is_spare: false}
    }

    pid = game.players.last.id.to_s.to_sym
    frames[pid] = { }

    #pp response.json_body

    assert_equal(response.code, "200")
    assert_equal(response.json_body, {
      game_id: game.id,
      start_at: game.start_at.to_json.gsub('"', ''),
      complete: !!game.complete,
      players: players_info,
      frames: frames,
      current_turn: {
        frame: 1,
        player_id: game.players.first.id,
        player_name: game.players.first.name,
        max_pins: 5
      }
    })
  end
end
