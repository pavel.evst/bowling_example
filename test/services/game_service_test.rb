require_relative "../test_helper"

describe GameService do
  make_my_diffs_pretty!

  DEFAULT_NAMES = %w{Alice Bob Carl}.freeze

  def defult_game_service(names: DEFAULT_NAMES)
    names = Array.wrap(names)
    game = Game.create!(player_names: names.join("\n"))
    GameService.new(game)
  end

  it "should rotate throw frames and rounds" do
    service = defult_game_service

    0.upto(59) do |i|
      expected_frame = ((i + 1) / 6.0).ceil
      expected_player = (i % 6) / 2

      #puts "Hit #{i} - frame #{expected_frame} - #{expected_player} #{names[expected_player]}"

      turn = service.current_turn

      assert_equal(turn[:frame], expected_frame)
      assert_equal(turn[:player].name, DEFAULT_NAMES[expected_player])

      service.save_hit(2)
    end

    assert_equal(service.game.complete, true)
  end

  it "should record scores and save winner" do
    service = defult_game_service

    0.upto(59) do |i|
      service.save_hit(rand(0..4))
    end

    assert_equal(service.game.complete, true)
    assert_equal(service.game.players.sort_by(&:score).last.winner, true)
    service.game.players.each do |player|
      assert_equal(player.score, service.player_scores(player.id).values.sum)
    end
  end

  describe "strike logic" do
    it "should record strike and skip 2nd hit in frame" do
      service = defult_game_service

      service.save_hit(10)
      turn = service.current_turn

      assert_equal(turn[:frame], 1)
      assert_equal(turn[:player].name, "Bob")

      last_hit = service.game.ball_throws.last
      assert_equal(last_hit.pins, 10)
      assert_equal(last_hit.is_strike, true)
      assert_equal(last_hit.is_spare, false)
    end

    it "should extend last frame if strike in 10th" do
      service = defult_game_service(names: "Melanie")

      18.times do
        service.save_hit(4)
      end

      service.save_hit(10)
      assert_equal(service.game.complete, false)
      assert_equal(service.current_turn[:max_pins], 10)

      service.save_hit(4)
      assert_equal(service.game.complete, false)
      assert_equal(service.current_turn[:max_pins], 6)

      service.save_hit(4)
      assert_equal(service.game.complete, true)
    end
  end

  describe "spare logic" do
    it "should record spare" do
      service = defult_game_service

      service.save_hit(2)
      service.save_hit(8)
      turn = service.current_turn

      assert_equal(turn[:frame], 1)
      assert_equal(turn[:player].name, "Bob")

      hits = service.game.ball_throws.order("id").to_a
      assert_equal(hits[1].pins, 8)
      assert_equal(hits[1].is_strike, false)
      assert_equal(hits[1].is_spare, true)
    end

    it "should extend last frame if spare in 10th" do
      service = defult_game_service(names: "Melanie")

      18.times do
        service.save_hit(4)
      end

      service.save_hit(4)
      assert_equal(service.game.complete, false)

      service.save_hit(6)
      assert_equal(service.game.complete, false)
      assert_equal(service.current_turn[:max_pins], 10)

      service.save_hit(4)
      assert_equal(service.game.complete, true)
    end
  end

  describe "scores" do
    it "should record strike scores" do
      service = defult_game_service(names: "Melanie")

      service.save_hit(10)

      18.times do
        service.save_hit(4)
      end

      # first frame = 10 + 4 + 4
      # subsequent frames = 8
      assert_equal(service.game.players.first.score, 90)
    end

    it "should record spare scores" do
      service = defult_game_service(names: "Melanie")

      service.save_hit(5)
      service.save_hit(5)

      18.times do
        service.save_hit(4)
      end

      # first frame = 5 + 5 + 4
      # subsequent frames = 8
      assert_equal(service.game.players.first.score, 86)
    end
  end
end
